## Tableau comparatif pour intégrer des framework JS avec Django

Ce projet vise à comparer différents frameworks JavaScript (Vue.JS, React et Angular) dans le but de les intégrer à Django.
Les informations dans le tableau sont tirées de divers forums, articles et documentations des frameworks JS eux-mêmes.
